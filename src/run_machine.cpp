#include "src/run_machine.h"
#include <QApplication>
#include <QDebug>

run_machine::run_machine(QString MachineName, QString SystemType, bool cd_image_set, QString CDImage , bool disk_image_set, QString DiskImage, QString RamSize, QString MachineType, bool enable_kvm, bool CPUType_set, QString CPUType, bool enable_spice, QString spice_port, bool enable_graphics, QString GraphicsCard, bool enable_networking, bool bypass_host_interface, QString NetworkInterface1, QString NetworkInterface2, QString NetworkInterface3) {
    if(QFile(DiskImage).exists()) {
        if(disk_image_set && QFile(DiskImage).exists() && enable_graphics && enable_spice && cd_image_set && enable_kvm && CPUType_set) {
                       program = "qemu-system-x86_64";
                       arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-cpu" << CPUType << "-vga" << GraphicsCard << "-cdrom" << CDImage << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-spice" <<  "port="+spice_port+",disable-ticketing" << "-device" << "virtio-serial-pci" << "-device" << "virtserialport,chardev=spicechannel0,name=com.redhat.spice.0" << "-chardev" << "spicevmc,id=spicechannel0,name=vdagent";
                       qDebug() << arguments;
                       MachineRunner = new QProcess();
                       MachineRunner->startDetached(program,arguments);
                       SpicyRunner = new QProcess();
                       program = "spicy";
                       SpicyArguments << "--title" << MachineName << "127.0.0.1" << "-p" << spice_port;
                       SpicyRunner->startDetached(program,SpicyArguments);
                }
                else if(disk_image_set && QFile(DiskImage).exists() && enable_graphics && enable_spice && cd_image_set && enable_kvm) {
                    program = "qemu-system-x86_64";
                    arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-vga" << GraphicsCard << "-cdrom" << CDImage << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-spice" <<  "port="+spice_port+",disable-ticketing" << "-device" << "virtio-serial-pci" << "-device" << "virtserialport,chardev=spicechannel0,name=com.redhat.spice.0" << "-chardev" << "spicevmc,id=spicechannel0,name=vdagent";
                    qDebug() << arguments;
                    MachineRunner = new QProcess();
                    MachineRunner->startDetached(program,arguments);
                    SpicyRunner = new QProcess();
                    program = "spicy";
                    SpicyArguments << "--title" << MachineName << "127.0.0.1" << "-p" << spice_port;
                    SpicyRunner->startDetached(program,SpicyArguments);
                }
                else if(disk_image_set && QFile(DiskImage).exists() && enable_graphics && enable_spice && enable_kvm) {
                    program = "qemu-system-x86_64";
                    arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-vga" << GraphicsCard << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-spice" <<  "port="+spice_port+",disable-ticketing" << "-device" << "virtio-serial-pci" << "-device" << "virtserialport,chardev=spicechannel0,name=com.redhat.spice.0" << "-chardev" << "spicevmc,id=spicechannel0,name=vdagent";
                    qDebug() << arguments;
                    MachineRunner = new QProcess();
                    MachineRunner->startDetached(program,arguments);
                    SpicyRunner = new QProcess();
                    program = "spicy";
                    SpicyArguments << "--title" << MachineName << "127.0.0.1" << "-p" << spice_port;
                    SpicyRunner->startDetached(program,SpicyArguments);
                }
                else if(disk_image_set && QFile(DiskImage).exists() && enable_graphics && enable_spice) {
                    program = "qemu-system-x86_64";
                    arguments << "-name" << MachineName << "-machine" << "type=" + MachineType << "-vga" << GraphicsCard << "-cdrom" << CDImage << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-spice" <<  "port="+spice_port+",disable-ticketing" << "-device" << "virtio-serial-pci" << "-device" << "virtserialport,chardev=spicechannel0,name=com.redhat.spice.0" << "-chardev" << "spicevmc,id=spicechannel0,name=vdagent";
                    qDebug() << arguments;
                    MachineRunner = new QProcess();
                    MachineRunner->startDetached(program,arguments);
                    SpicyRunner = new QProcess();
                    program = "spicy";
                    SpicyArguments << "--title" << MachineName << "127.0.0.1" << "-p" << spice_port;
                    SpicyRunner->startDetached(program,SpicyArguments);
                }
                else if(disk_image_set && QFile(DiskImage).exists() && enable_graphics && enable_spice && enable_kvm) {
                    program = "qemu-system-x86_64";
                    arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-vga" << GraphicsCard << "-cdrom" << CDImage << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-spice" <<  "port="+spice_port+",disable-ticketing" << "-device" << "virtio-serial-pci" << "-device" << "virtserialport,chardev=spicechannel0,name=com.redhat.spice.0" << "-chardev" << "spicevmc,id=spicechannel0,name=vdagent";
                    qDebug() << arguments;
                    MachineRunner = new QProcess();
                    MachineRunner->startDetached(program,arguments);
                    SpicyRunner = new QProcess();
                    program = "spicy";
                    SpicyArguments << "--title" << MachineName << "127.0.0.1" << "-p" << spice_port;
                    SpicyRunner->startDetached(program,SpicyArguments);
                }
                else if(disk_image_set && QFile(DiskImage).exists() && enable_graphics && enable_spice==false && enable_kvm) {
                    program = "qemu-system-x86_64";
                    arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-vga" << GraphicsCard << "-cdrom" << CDImage << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-device" << "virtio-serial-pci";
                    qDebug() << arguments;
                    MachineRunner = new QProcess();
                    MachineRunner->startDetached(program,arguments);
                }
                else if(disk_image_set && QFile(DiskImage).exists() && enable_graphics && enable_spice==false) {
                    program = "qemu-system-x86_64";
                    arguments << "-name" << MachineName << "-machine" << "type=" + MachineType << "-vga" << GraphicsCard << "-cdrom" << CDImage << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-device" << "virtio-serial-pci";
                    qDebug() << arguments;
                    MachineRunner = new QProcess();
                    MachineRunner->startDetached(program,arguments);
                }
                else if(disk_image_set && QFile(DiskImage).exists() && enable_graphics && enable_spice==false && enable_kvm) {
                    program = "qemu-system-x86_64";
                    arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-vga" << GraphicsCard << "-cdrom" << CDImage << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-device" << "virtio-serial-pci";
                    qDebug() << arguments;
                    MachineRunner = new QProcess();
                    MachineRunner->startDetached(program,arguments);
                }
                else if(disk_image_set && QFile(DiskImage).exists() && enable_kvm) {
                    program = "qemu-system-x86_64";
                    arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-vga" << GraphicsCard << "-cdrom" << CDImage << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-device" << "virtio-serial-pci";
                    qDebug() << arguments;
                    MachineRunner = new QProcess();
                    MachineRunner->startDetached(program,arguments);
                }


        if(disk_image_set && QFile(DiskImage).exists() && enable_graphics && enable_spice && cd_image_set && enable_kvm && CPUType_set  && enable_networking) {
                    program = "qemu-system-x86_64";
                    if(NetworkInterface1!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-cpu" << CPUType << "-vga" << GraphicsCard << "-cdrom" << CDImage << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 << "-spice" <<  "port="+spice_port+",disable-ticketing" << "-device" << "virtio-serial-pci" << "-device" << "virtserialport,chardev=spicechannel0,name=com.redhat.spice.0" << "-chardev" << "spicevmc,id=spicechannel0,name=vdagent";
                    }
                    else if(NetworkInterface1!="" && NetworkInterface2!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-cpu" << CPUType << "-vga" << GraphicsCard << "-cdrom" << CDImage << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 << "-net" << "nic,model=" + NetworkInterface2 << "-spice" <<  "port="+spice_port+",disable-ticketing" << "-device" << "virtio-serial-pci" << "-device" << "virtserialport,chardev=spicechannel0,name=com.redhat.spice.0" << "-chardev" << "spicevmc,id=spicechannel0,name=vdagent";
                    }
                    else if(NetworkInterface1!="" && NetworkInterface2!="" && NetworkInterface3!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-cpu" << CPUType << "-vga" << GraphicsCard << "-cdrom" << CDImage << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 <<  "nic,model=" + NetworkInterface2 <<  "nic,model=" + NetworkInterface3 << "-spice" <<  "port="+spice_port+",disable-ticketing" << "-device" << "virtio-serial-pci" << "-device" << "virtserialport,chardev=spicechannel0,name=com.redhat.spice.0" << "-chardev" << "spicevmc,id=spicechannel0,name=vdagent";
                    }
                    qDebug() << arguments;
                    MachineRunner = new QProcess();
                    MachineRunner->startDetached(program,arguments);
                    SpicyRunner = new QProcess();
                    program = "spicy";
                    SpicyArguments << "--title" << MachineName << "127.0.0.1" << "-p" << spice_port;
                    SpicyRunner->startDetached(program,SpicyArguments);
                }
                else if(disk_image_set && QFile(DiskImage).exists() && enable_graphics && enable_spice && cd_image_set && enable_kvm && enable_networking) {
                    program = "qemu-system-x86_64";
                    if(NetworkInterface1!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-vga" << GraphicsCard << "-cdrom" << CDImage << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 << "-spice" <<  "port="+spice_port+",disable-ticketing" << "-device" << "virtio-serial-pci" << "-device" << "virtserialport,chardev=spicechannel0,name=com.redhat.spice.0" << "-chardev" << "spicevmc,id=spicechannel0,name=vdagent";
                    }
                    else if(NetworkInterface1!="" && NetworkInterface2!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-vga" << GraphicsCard << "-cdrom" << CDImage << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 << "-net" << "nic,model=" + NetworkInterface2 << "-spice" <<  "port="+spice_port+",disable-ticketing" << "-device" << "virtio-serial-pci" << "-device" << "virtserialport,chardev=spicechannel0,name=com.redhat.spice.0" << "-chardev" << "spicevmc,id=spicechannel0,name=vdagent";
                    }
                    else if(NetworkInterface1!="" && NetworkInterface2!="" && NetworkInterface3!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-vga" << GraphicsCard << "-cdrom" << CDImage << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 <<  "nic,model=" + NetworkInterface2 <<  "nic,model=" + NetworkInterface3 << "-spice" <<  "port="+spice_port+",disable-ticketing" << "-device" << "virtio-serial-pci" << "-device" << "virtserialport,chardev=spicechannel0,name=com.redhat.spice.0" << "-chardev" << "spicevmc,id=spicechannel0,name=vdagent";
                    }
                    qDebug() << arguments;
                    MachineRunner = new QProcess();
                    MachineRunner->startDetached(program,arguments);
                    SpicyRunner = new QProcess();
                    program = "spicy";
                    SpicyArguments << "--title" << MachineName << "127.0.0.1" << "-p" << spice_port;
                    SpicyRunner->startDetached(program,SpicyArguments);
                }

        else if(disk_image_set && QFile(DiskImage).exists() && enable_graphics && enable_spice && enable_kvm && CPUType_set  && enable_networking) {
                    program = "qemu-system-x86_64";
                    if(NetworkInterface1!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-cpu" << CPUType << "-vga" << GraphicsCard << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 << "-spice" <<  "port="+spice_port+",disable-ticketing" << "-device" << "virtio-serial-pci" << "-device" << "virtserialport,chardev=spicechannel0,name=com.redhat.spice.0" << "-chardev" << "spicevmc,id=spicechannel0,name=vdagent";
                    }
                    else if(NetworkInterface1!="" && NetworkInterface2!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-cpu" << CPUType << "-vga" << GraphicsCard << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 << "-net" << "nic,model=" + NetworkInterface2 << "-spice" <<  "port="+spice_port+",disable-ticketing" << "-device" << "virtio-serial-pci" << "-device" << "virtserialport,chardev=spicechannel0,name=com.redhat.spice.0" << "-chardev" << "spicevmc,id=spicechannel0,name=vdagent";
                    }
                    else if(NetworkInterface1!="" && NetworkInterface2!="" && NetworkInterface3!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-cpu" << CPUType << "-vga" << GraphicsCard << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 <<  "nic,model=" + NetworkInterface2 <<  "nic,model=" + NetworkInterface3 << "-spice" <<  "port="+spice_port+",disable-ticketing" << "-device" << "virtio-serial-pci" << "-device" << "virtserialport,chardev=spicechannel0,name=com.redhat.spice.0" << "-chardev" << "spicevmc,id=spicechannel0,name=vdagent";
                    }
                    qDebug() << arguments;
                    MachineRunner = new QProcess();
                    MachineRunner->startDetached(program,arguments);
                    SpicyRunner = new QProcess();
                    program = "spicy";
                    SpicyArguments << "--title" << MachineName << "127.0.0.1" << "-p" << spice_port;
                    SpicyRunner->startDetached(program,SpicyArguments);
                }
                else if(disk_image_set && QFile(DiskImage).exists() && enable_graphics && enable_spice && enable_kvm && enable_networking) {
                    program = "qemu-system-x86_64";
                    if(NetworkInterface1!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-vga" << GraphicsCard << "-cdrom" << CDImage << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 << "-spice" <<  "port="+spice_port+",disable-ticketing" << "-device" << "virtio-serial-pci" << "-device" << "virtserialport,chardev=spicechannel0,name=com.redhat.spice.0" << "-chardev" << "spicevmc,id=spicechannel0,name=vdagent";
                    }
                    else if(NetworkInterface1!="" && NetworkInterface2!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-vga" << GraphicsCard << "-cdrom" << CDImage << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 << "-net" << "nic,model=" + NetworkInterface2 << "-spice" <<  "port="+spice_port+",disable-ticketing" << "-device" << "virtio-serial-pci" << "-device" << "virtserialport,chardev=spicechannel0,name=com.redhat.spice.0" << "-chardev" << "spicevmc,id=spicechannel0,name=vdagent";
                    }
                    else if(NetworkInterface1!="" && NetworkInterface2!="" && NetworkInterface3!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-vga" << GraphicsCard << "-cdrom" << CDImage << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 <<  "nic,model=" + NetworkInterface2 <<  "nic,model=" + NetworkInterface3 << "-spice" <<  "port="+spice_port+",disable-ticketing" << "-device" << "virtio-serial-pci" << "-device" << "virtserialport,chardev=spicechannel0,name=com.redhat.spice.0" << "-chardev" << "spicevmc,id=spicechannel0,name=vdagent";
                    }
                    qDebug() << arguments;
                    MachineRunner = new QProcess();
                    MachineRunner->startDetached(program,arguments);
                    SpicyRunner = new QProcess();
                    program = "spicy";
                    SpicyArguments << "--title" << MachineName << "127.0.0.1" << "-p" << spice_port;
                    SpicyRunner->startDetached(program,SpicyArguments);
                }

                else if(disk_image_set && QFile(DiskImage).exists() && enable_graphics && enable_kvm && enable_networking) {
                    program = "qemu-system-x86_64";
                    if(NetworkInterface1!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-vga" << GraphicsCard << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 << "-device" << "virtio-serial-pci";
                    }
                    else if(NetworkInterface1!="" && NetworkInterface2!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-vga" << GraphicsCard << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 << "-net" << "nic,model=" + NetworkInterface2 << "-device" << "virtio-serial-pci";
                    }
                    else if(NetworkInterface1!="" && NetworkInterface2!="" && NetworkInterface3!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-vga" << GraphicsCard << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 <<  "nic,model=" + NetworkInterface2 <<  "nic,model=" + NetworkInterface3 << "-device" << "virtio-serial-pci";
                    }
                    qDebug() << arguments;
                    MachineRunner = new QProcess();
                    MachineRunner->startDetached(program,arguments);
                    SpicyRunner = new QProcess();
                    program = "spicy";
                    SpicyArguments << "--title" << MachineName << "127.0.0.1" << "-p" << spice_port;
                    SpicyRunner->startDetached(program,SpicyArguments);
                }
                else if(disk_image_set && QFile(DiskImage).exists() && enable_graphics && enable_spice && enable_networking) {
                    program = "qemu-system-x86_64";
                    if(NetworkInterface1!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType << "-vga" << GraphicsCard << "-cdrom" << CDImage << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 << "-spice" <<  "port="+spice_port+",disable-ticketing" << "-device" << "virtio-serial-pci" << "-device" << "virtserialport,chardev=spicechannel0,name=com.redhat.spice.0" << "-chardev" << "spicevmc,id=spicechannel0,name=vdagent";
                    }
                    else if(NetworkInterface1!="" && NetworkInterface2!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-vga" << GraphicsCard << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 << "-net" << "nic,model=" + NetworkInterface2 << "-spice" <<  "port="+spice_port+",disable-ticketing" << "-device" << "virtio-serial-pci" << "-device" << "virtserialport,chardev=spicechannel0,name=com.redhat.spice.0" << "-chardev" << "spicevmc,id=spicechannel0,name=vdagent";
                    }
                    else if(NetworkInterface1!="" && NetworkInterface2!="" && NetworkInterface3!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-vga" << GraphicsCard << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 <<  "-net" << "nic,model=" + NetworkInterface2 << "-net" << "nic,model=" + NetworkInterface3 << "-spice" <<  "port="+spice_port+",disable-ticketing" << "-device" << "virtio-serial-pci" << "-device" << "virtserialport,chardev=spicechannel0,name=com.redhat.spice.0" << "-chardev" << "spicevmc,id=spicechannel0,name=vdagent";
                    }
                    qDebug() << arguments;
                    MachineRunner = new QProcess();
                    MachineRunner->startDetached(program,arguments);
                    SpicyRunner = new QProcess();
                    program = "spicy";
                    SpicyArguments << "--title" << MachineName << "127.0.0.1" << "-p" << spice_port;
                    SpicyRunner->startDetached(program,SpicyArguments);
                }
                else if(disk_image_set && QFile(DiskImage).exists() && enable_graphics && enable_spice && enable_kvm && enable_networking) {
                    program = "qemu-system-x86_64";
                    if(NetworkInterface1!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-vga" << GraphicsCard << "-cdrom" << CDImage << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 << "-spice" <<  "port="+spice_port+",disable-ticketing" << "-device" << "virtio-serial-pci" << "-device" << "virtserialport,chardev=spicechannel0,name=com.redhat.spice.0" << "-chardev" << "spicevmc,id=spicechannel0,name=vdagent";
                    }
                    else if(NetworkInterface1!="" && NetworkInterface2!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-vga" << GraphicsCard << "-cdrom" << CDImage << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 << "-net" << "nic,model=" + NetworkInterface2 << "-spice" <<  "port="+spice_port+",disable-ticketing" << "-device" << "virtio-serial-pci" << "-device" << "virtserialport,chardev=spicechannel0,name=com.redhat.spice.0" << "-chardev" << "spicevmc,id=spicechannel0,name=vdagent";
                    }
                    else if(NetworkInterface1!="" && NetworkInterface2!="" && NetworkInterface3!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-vga" << GraphicsCard << "-cdrom" << CDImage << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 <<  "-net" << "nic,model=" + NetworkInterface2 << "-net" << "nic,model=" + NetworkInterface3 << "-spice" <<  "port="+spice_port+",disable-ticketing" << "-device" << "virtio-serial-pci" << "-device" << "virtserialport,chardev=spicechannel0,name=com.redhat.spice.0" << "-chardev" << "spicevmc,id=spicechannel0,name=vdagent";
                    }
                    qDebug() << arguments;
                    MachineRunner = new QProcess();
                    MachineRunner->startDetached(program,arguments);
                    SpicyRunner = new QProcess();
                    program = "spicy";
                    SpicyArguments << "--title" << MachineName << "127.0.0.1" << "-p" << spice_port;
                    SpicyRunner->startDetached(program,SpicyArguments);
                }
                else if(disk_image_set && QFile(DiskImage).exists() && enable_graphics && enable_spice==false && enable_kvm && enable_networking) {
                    program = "qemu-system-x86_64";
                    if(NetworkInterface1!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-vga" << GraphicsCard << "-cdrom" << CDImage << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 << "-spice" <<  "port="+spice_port+",disable-ticketing" << "-device" << "virtio-serial-pci" << "-device" << "virtserialport,chardev=spicechannel0,name=com.redhat.spice.0" << "-chardev" << "spicevmc,id=spicechannel0,name=vdagent";
                    }
                    else if(NetworkInterface1!="" && NetworkInterface2!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-vga" << GraphicsCard << "-cdrom" << CDImage << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 << "-net" << "nic,model=" + NetworkInterface2 << "-spice" <<  "port="+spice_port+",disable-ticketing" << "-device" << "virtio-serial-pci" << "-device" << "virtserialport,chardev=spicechannel0,name=com.redhat.spice.0" << "-chardev" << "spicevmc,id=spicechannel0,name=vdagent";
                    }
                    else if(NetworkInterface1!="" && NetworkInterface2!="" && NetworkInterface3!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-vga" << GraphicsCard << "-cdrom" << CDImage << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 <<  "-net" << "nic,model=" + NetworkInterface2 << "-net" << "nic,model=" + NetworkInterface3 << "-spice" <<  "port="+spice_port+",disable-ticketing" << "-device" << "virtio-serial-pci" << "-device" << "virtserialport,chardev=spicechannel0,name=com.redhat.spice.0" << "-chardev" << "spicevmc,id=spicechannel0,name=vdagent";
                    }
                    qDebug() << arguments;
                    MachineRunner = new QProcess();
                    MachineRunner->startDetached(program,arguments);
                }
                else if(disk_image_set && QFile(DiskImage).exists() && enable_graphics && enable_spice==false && enable_networking) {
                    program = "qemu-system-x86_64";
                    if(NetworkInterface1!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType << "-vga" << GraphicsCard << "-cdrom" << CDImage << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 << "-device" << "virtio-serial-pci";
                    }
                    else if(NetworkInterface1!="" && NetworkInterface2!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType << "-vga" << GraphicsCard << "-cdrom" << CDImage << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 << "-net" << "nic,model=" + NetworkInterface2 << "-device" << "virtio-serial-pci";
                    }
                    else if(NetworkInterface1!="" && NetworkInterface2!="" && NetworkInterface3!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType << "-vga" << GraphicsCard << "-cdrom" << CDImage << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 << "-net" << "nic,model=" + NetworkInterface2 << "-net" << "nic,model=" + NetworkInterface3 << "-device" << "virtio-serial-pci";
                    }
                    qDebug() << arguments;
                    MachineRunner = new QProcess();
                    MachineRunner->startDetached(program,arguments);
                }
                else if(disk_image_set && QFile(DiskImage).exists() && enable_graphics && enable_spice==false && enable_kvm && enable_networking) {
                    program = "qemu-system-x86_64";
                    if(NetworkInterface1!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-vga" << GraphicsCard << "-cdrom" << CDImage << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 << "-device" << "virtio-serial-pci";
                    }
                    else if(NetworkInterface1!="" && NetworkInterface2!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-vga" << GraphicsCard << "-cdrom" << CDImage << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 << "-net" << "nic,model=" + NetworkInterface2 << "-device" << "virtio-serial-pci";
                    }
                    else if(NetworkInterface1!="" && NetworkInterface2!="" && NetworkInterface3!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-vga" << GraphicsCard << "-cdrom" << CDImage << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 << "-net" << "nic,model=" + NetworkInterface2 << "-net" << "nic,model=" + NetworkInterface3 << "-device" << "virtio-serial-pci";
                    }
                    qDebug() << arguments;
                    MachineRunner = new QProcess();
                    MachineRunner->startDetached(program,arguments);
                }





        if(disk_image_set && QFile(DiskImage).exists() && enable_graphics && enable_spice && cd_image_set && enable_kvm && CPUType_set && enable_networking && bypass_host_interface) {
                    program = "qemu-system-x86_64";
                    if(NetworkInterface1!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-cpu" << CPUType << "-vga" << GraphicsCard << "-cdrom" << CDImage << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 << "-net" << "user" << "-spice" <<  "port="+spice_port+",disable-ticketing" << "-device" << "virtio-serial-pci" << "-device" << "virtserialport,chardev=spicechannel0,name=com.redhat.spice.0" << "-chardev" << "spicevmc,id=spicechannel0,name=vdagent";
                    }
                    else if(NetworkInterface1!="" && NetworkInterface2!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-cpu" << CPUType << "-vga" << GraphicsCard << "-cdrom" << CDImage << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 << "-net" << "nic,model=" + NetworkInterface2  << "-net" <<  "user" << "-spice" <<  "port="+spice_port+",disable-ticketing" << "-device" << "virtio-serial-pci" << "-device" << "virtserialport,chardev=spicechannel0,name=com.redhat.spice.0" << "-chardev" << "spicevmc,id=spicechannel0,name=vdagent";
                    }
                    else if(NetworkInterface1!="" && NetworkInterface2!="" && NetworkInterface3!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-cpu" << CPUType << "-vga" << GraphicsCard << "-cdrom" << CDImage << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 <<  "nic,model=" + NetworkInterface2 <<  "nic,model=" + NetworkInterface3 << "-net" <<  "user" << "-spice" <<  "port="+spice_port+",disable-ticketing" << "-device" << "virtio-serial-pci" << "-device" << "virtserialport,chardev=spicechannel0,name=com.redhat.spice.0" << "-chardev" << "spicevmc,id=spicechannel0,name=vdagent";
                    }
                    qDebug() << arguments;
                    MachineRunner = new QProcess();
                    MachineRunner->startDetached(program,arguments);
                    SpicyRunner = new QProcess();
                    program = "spicy";
                    SpicyArguments << "--title" << MachineName << "127.0.0.1" << "-p" << spice_port;
                    SpicyRunner->startDetached(program,SpicyArguments);
                }
                else if(disk_image_set && QFile(DiskImage).exists() && enable_graphics && enable_spice && cd_image_set && enable_kvm && enable_networking && bypass_host_interface) {
                    program = "qemu-system-x86_64";
                    if(NetworkInterface1!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-vga" << GraphicsCard << "-cdrom" << CDImage << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 << "-net" << "user" << "-spice" <<  "port="+spice_port+",disable-ticketing" << "-device" << "virtio-serial-pci" << "-device" << "virtserialport,chardev=spicechannel0,name=com.redhat.spice.0" << "-chardev" << "spicevmc,id=spicechannel0,name=vdagent";
                    }
                    else if(NetworkInterface1!="" && NetworkInterface2!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-vga" << GraphicsCard << "-cdrom" << CDImage << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 << "-net" << "nic,model=" + NetworkInterface2 << "-net" << "user" << "-spice" <<  "port="+spice_port+",disable-ticketing" << "-device" << "virtio-serial-pci" << "-device" << "virtserialport,chardev=spicechannel0,name=com.redhat.spice.0" << "-chardev" << "spicevmc,id=spicechannel0,name=vdagent";
                    }
                    else if(NetworkInterface1!="" && NetworkInterface2!="" && NetworkInterface3!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-vga" << GraphicsCard << "-cdrom" << CDImage << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 <<  "nic,model=" + NetworkInterface2 <<  "nic,model=" + NetworkInterface3 << "-net" << "user" << "-spice" <<  "port="+spice_port+",disable-ticketing" << "-device" << "virtio-serial-pci" << "-device" << "virtserialport,chardev=spicechannel0,name=com.redhat.spice.0" << "-chardev" << "spicevmc,id=spicechannel0,name=vdagent";
                    }
                    qDebug() << arguments;
                    MachineRunner = new QProcess();
                    MachineRunner->startDetached(program,arguments);
                    SpicyRunner = new QProcess();
                    program = "spicy";
                    SpicyArguments << "--title" << MachineName << "127.0.0.1" << "-p" << spice_port;
                    SpicyRunner->startDetached(program,SpicyArguments);
                }

        else if(disk_image_set && QFile(DiskImage).exists() && enable_graphics && enable_spice && enable_kvm && CPUType_set  && enable_networking && bypass_host_interface) {
                    program = "qemu-system-x86_64";
                    if(NetworkInterface1!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-cpu" << CPUType << "-vga" << GraphicsCard << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 << "-net" << "user" << "-spice" <<  "port="+spice_port+",disable-ticketing" << "-device" << "virtio-serial-pci" << "-device" << "virtserialport,chardev=spicechannel0,name=com.redhat.spice.0" << "-chardev" << "spicevmc,id=spicechannel0,name=vdagent";
                    }
                    else if(NetworkInterface1!="" && NetworkInterface2!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-cpu" << CPUType << "-vga" << GraphicsCard << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 << "-net" << "nic,model=" + NetworkInterface2 << "-net" << "user" << "-spice" <<  "port="+spice_port+",disable-ticketing" << "-device" << "virtio-serial-pci" << "-device" << "virtserialport,chardev=spicechannel0,name=com.redhat.spice.0" << "-chardev" << "spicevmc,id=spicechannel0,name=vdagent";
                    }
                    else if(NetworkInterface1!="" && NetworkInterface2!="" && NetworkInterface3!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-cpu" << CPUType << "-vga" << GraphicsCard << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 <<  "nic,model=" + NetworkInterface2 <<  "nic,model=" + NetworkInterface3 << "-net" << "user" << "-spice" <<  "port="+spice_port+",disable-ticketing" << "-device" << "virtio-serial-pci" << "-device" << "virtserialport,chardev=spicechannel0,name=com.redhat.spice.0" << "-chardev" << "spicevmc,id=spicechannel0,name=vdagent";
                    }
                    qDebug() << arguments;
                    MachineRunner = new QProcess();
                    MachineRunner->startDetached(program,arguments);
                    SpicyRunner = new QProcess();
                    program = "spicy";
                    SpicyArguments << "--title" << MachineName << "127.0.0.1" << "-p" << spice_port;
                    SpicyRunner->startDetached(program,SpicyArguments);
                }
                else if(disk_image_set && QFile(DiskImage).exists() && enable_graphics && enable_spice && enable_kvm && enable_networking && bypass_host_interface) {
                    program = "qemu-system-x86_64";
                    if(NetworkInterface1!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-vga" << GraphicsCard << "-cdrom" << CDImage << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 << "-net" << "user" << "-spice" <<  "port="+spice_port+",disable-ticketing" << "-device" << "virtio-serial-pci" << "-device" << "virtserialport,chardev=spicechannel0,name=com.redhat.spice.0" << "-chardev" << "spicevmc,id=spicechannel0,name=vdagent";
                    }
                    else if(NetworkInterface1!="" && NetworkInterface2!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-vga" << GraphicsCard << "-cdrom" << CDImage << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 << "-net" << "nic,model=" + NetworkInterface2 << "-net" << "user" << "-spice" <<  "port="+spice_port+",disable-ticketing" << "-device" << "virtio-serial-pci" << "-device" << "virtserialport,chardev=spicechannel0,name=com.redhat.spice.0" << "-chardev" << "spicevmc,id=spicechannel0,name=vdagent";
                    }
                    else if(NetworkInterface1!="" && NetworkInterface2!="" && NetworkInterface3!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-vga" << GraphicsCard << "-cdrom" << CDImage << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 <<  "nic,model=" + NetworkInterface2 <<  "nic,model=" + NetworkInterface3 << "-net" << "user" << "-spice" <<  "port="+spice_port+",disable-ticketing" << "-device" << "virtio-serial-pci" << "-device" << "virtserialport,chardev=spicechannel0,name=com.redhat.spice.0" << "-chardev" << "spicevmc,id=spicechannel0,name=vdagent";
                    }
                    qDebug() << arguments;
                    MachineRunner = new QProcess();
                    MachineRunner->startDetached(program,arguments);
                    SpicyRunner = new QProcess();
                    program = "spicy";
                    SpicyArguments << "--title" << MachineName << "127.0.0.1" << "-p" << spice_port;
                    SpicyRunner->startDetached(program,SpicyArguments);
                }

                else if(disk_image_set && QFile(DiskImage).exists() && enable_graphics && enable_kvm && enable_networking && bypass_host_interface) {
                    program = "qemu-system-x86_64";
                    if(NetworkInterface1!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-vga" << GraphicsCard << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 << "-net" << "user" << "-device" << "virtio-serial-pci";
                    }
                    else if(NetworkInterface1!="" && NetworkInterface2!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-vga" << GraphicsCard << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 << "-net" << "nic,model=" + NetworkInterface2 << "-net" << "user" << "-device" << "virtio-serial-pci";
                    }
                    else if(NetworkInterface1!="" && NetworkInterface2!="" && NetworkInterface3!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-vga" << GraphicsCard << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 <<  "nic,model=" + NetworkInterface2 <<  "nic,model=" + NetworkInterface3 << "-net" << "user" << "-device" << "virtio-serial-pci";
                    }
                    qDebug() << arguments;
                    MachineRunner = new QProcess();
                    MachineRunner->startDetached(program,arguments);
                    SpicyRunner = new QProcess();
                    program = "spicy";
                    SpicyArguments << "--title" << MachineName << "127.0.0.1" << "-p" << spice_port;
                    SpicyRunner->startDetached(program,SpicyArguments);
                }
                else if(disk_image_set && QFile(DiskImage).exists() && enable_graphics && enable_spice && enable_networking && bypass_host_interface) {
                    program = "qemu-system-x86_64";
                    if(NetworkInterface1!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType << "-vga" << GraphicsCard << "-cdrom" << CDImage << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 << "-net" << "user" << "-spice" <<  "port="+spice_port+",disable-ticketing" << "-device" << "virtio-serial-pci" << "-device" << "virtserialport,chardev=spicechannel0,name=com.redhat.spice.0" << "-chardev" << "spicevmc,id=spicechannel0,name=vdagent";
                    }
                    else if(NetworkInterface1!="" && NetworkInterface2!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-vga" << GraphicsCard << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 << "-net" << "nic,model=" + NetworkInterface2 << "-net" << "user" << "-spice" <<  "port="+spice_port+",disable-ticketing" << "-device" << "virtio-serial-pci" << "-device" << "virtserialport,chardev=spicechannel0,name=com.redhat.spice.0" << "-chardev" << "spicevmc,id=spicechannel0,name=vdagent";
                    }
                    else if(NetworkInterface1!="" && NetworkInterface2!="" && NetworkInterface3!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-vga" << GraphicsCard << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 <<  "-net" << "nic,model=" + NetworkInterface2 << "-net" << "nic,model=" + NetworkInterface3 << "-net" << "user" << "-spice" <<  "port="+spice_port+",disable-ticketing" << "-device" << "virtio-serial-pci" << "-device" << "virtserialport,chardev=spicechannel0,name=com.redhat.spice.0" << "-chardev" << "spicevmc,id=spicechannel0,name=vdagent";
                    }
                    qDebug() << arguments;
                    MachineRunner = new QProcess();
                    MachineRunner->startDetached(program,arguments);
                    SpicyRunner = new QProcess();
                    program = "spicy";
                    SpicyArguments << "--title" << MachineName << "127.0.0.1" << "-p" << spice_port;
                    SpicyRunner->startDetached(program,SpicyArguments);
                }
                else if(disk_image_set && QFile(DiskImage).exists() && enable_graphics && enable_spice && enable_kvm && enable_networking && bypass_host_interface) {
                    program = "qemu-system-x86_64";
                    if(NetworkInterface1!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-vga" << GraphicsCard << "-cdrom" << CDImage << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 << "-net" << "user" << "-spice" <<  "port="+spice_port+",disable-ticketing" << "-device" << "virtio-serial-pci" << "-device" << "virtserialport,chardev=spicechannel0,name=com.redhat.spice.0" << "-chardev" << "spicevmc,id=spicechannel0,name=vdagent";
                    }
                    else if(NetworkInterface1!="" && NetworkInterface2!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-vga" << GraphicsCard << "-cdrom" << CDImage << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 << "-net" << "nic,model=" + NetworkInterface2 << "-net" << "user" << "-spice" <<  "port="+spice_port+",disable-ticketing" << "-device" << "virtio-serial-pci" << "-device" << "virtserialport,chardev=spicechannel0,name=com.redhat.spice.0" << "-chardev" << "spicevmc,id=spicechannel0,name=vdagent";
                    }
                    else if(NetworkInterface1!="" && NetworkInterface2!="" && NetworkInterface3!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-vga" << GraphicsCard << "-cdrom" << CDImage << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 <<  "-net" << "nic,model=" + NetworkInterface2 << "-net" << "nic,model=" + NetworkInterface3 << "-net" << "user" << "-spice" <<  "port="+spice_port+",disable-ticketing" << "-device" << "virtio-serial-pci" << "-device" << "virtserialport,chardev=spicechannel0,name=com.redhat.spice.0" << "-chardev" << "spicevmc,id=spicechannel0,name=vdagent";
                    }
                    qDebug() << arguments;
                    MachineRunner = new QProcess();
                    MachineRunner->startDetached(program,arguments);
                    SpicyRunner = new QProcess();
                    program = "spicy";
                    SpicyArguments << "--title" << MachineName << "127.0.0.1" << "-p" << spice_port;
                    SpicyRunner->startDetached(program,SpicyArguments);
                }
                else if(disk_image_set && QFile(DiskImage).exists() && enable_graphics && enable_spice==false && enable_kvm && enable_networking && bypass_host_interface) {
                    program = "qemu-system-x86_64";
                    if(NetworkInterface1!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-vga" << GraphicsCard << "-cdrom" << CDImage << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 << "-net" << "user" << "-spice" <<  "port="+spice_port+",disable-ticketing" << "-device" << "virtio-serial-pci" << "-device" << "virtserialport,chardev=spicechannel0,name=com.redhat.spice.0" << "-chardev" << "spicevmc,id=spicechannel0,name=vdagent";
                    }
                    else if(NetworkInterface1!="" && NetworkInterface2!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-vga" << GraphicsCard << "-cdrom" << CDImage << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 << "-net" << "nic,model=" + NetworkInterface2 << "-net" << "user" << "-spice" <<  "port="+spice_port+",disable-ticketing" << "-device" << "virtio-serial-pci" << "-device" << "virtserialport,chardev=spicechannel0,name=com.redhat.spice.0" << "-chardev" << "spicevmc,id=spicechannel0,name=vdagent";
                    }
                    else if(NetworkInterface1!="" && NetworkInterface2!="" && NetworkInterface3!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-vga" << GraphicsCard << "-cdrom" << CDImage << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 <<  "-net" << "nic,model=" + NetworkInterface2 << "-net" << "nic,model=" + NetworkInterface3 << "-net" << "user" << "-spice" <<  "port="+spice_port+",disable-ticketing" << "-device" << "virtio-serial-pci" << "-device" << "virtserialport,chardev=spicechannel0,name=com.redhat.spice.0" << "-chardev" << "spicevmc,id=spicechannel0,name=vdagent";
                    }
                    qDebug() << arguments;
                    MachineRunner = new QProcess();
                    MachineRunner->startDetached(program,arguments);
                }
                else if(disk_image_set && QFile(DiskImage).exists() && enable_graphics && enable_spice==false && enable_networking && bypass_host_interface) {
                    program = "qemu-system-x86_64";
                    if(NetworkInterface1!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType << "-vga" << GraphicsCard << "-cdrom" << CDImage << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 << "-net" << "user" << "-device" << "virtio-serial-pci";
                    }
                    else if(NetworkInterface1!="" && NetworkInterface2!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType << "-vga" << GraphicsCard << "-cdrom" << CDImage << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 << "-net" << "nic,model=" + NetworkInterface2 << "-net" << "user" << "-device" << "virtio-serial-pci";
                    }
                    else if(NetworkInterface1!="" && NetworkInterface2!="" && NetworkInterface3!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType << "-vga" << GraphicsCard << "-cdrom" << CDImage << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 << "-net" << "nic,model=" + NetworkInterface2 << "-net" << "nic,model=" + NetworkInterface3 << "-net" << "user" << "-device" << "virtio-serial-pci";
                    }
                    qDebug() << arguments;
                    MachineRunner = new QProcess();
                    MachineRunner->startDetached(program,arguments);
                }
                else if(disk_image_set && QFile(DiskImage).exists() && enable_graphics && enable_spice==false && enable_kvm && enable_networking && bypass_host_interface) {
                    program = "qemu-system-x86_64";
                    if(NetworkInterface1!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-vga" << GraphicsCard << "-cdrom" << CDImage << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 << "-net" << "user" << "-device" << "virtio-serial-pci";
                    }
                    else if(NetworkInterface1!="" && NetworkInterface2!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-vga" << GraphicsCard << "-cdrom" << CDImage << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 << "-net" << "nic,model=" + NetworkInterface2 << "-net" << "user" << "-device" << "virtio-serial-pci";
                    }
                    else if(NetworkInterface1!="" && NetworkInterface2!="" && NetworkInterface3!="") {
                        arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-vga" << GraphicsCard << "-cdrom" << CDImage << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-net" << "nic,model=" + NetworkInterface1 << "-net" << "nic,model=" + NetworkInterface2 << "-net" << "nic,model=" + NetworkInterface3 << "-net" << "user" << "-device" << "virtio-serial-pci";
                    }
                    qDebug() << arguments;
                    MachineRunner = new QProcess();
                    MachineRunner->startDetached(program,arguments);
                }

        else if(disk_image_set && QFile(DiskImage).exists() && enable_kvm) {
            program = "qemu-system-x86_64";
            arguments << "-name" << MachineName << "-machine" << "type=" + MachineType + ",accel=kvm" << "--enable-kvm" << "-vga" << GraphicsCard << "-cdrom" << CDImage << "-drive" <<  "file=" + DiskImage << "-m" << RamSize << "-device" << "virtio-serial-pci";
            qDebug() << arguments;
            MachineRunner = new QProcess();
            MachineRunner->startDetached(program,arguments);
        }

    } else {
        DiskImageError.showMessage("The specified Disk Image does not exist!");
        DiskImageError.exec();
        qApp->quit();
        QProcess::startDetached(qApp->arguments()[0], qApp->arguments());
    }

}


run_machine::~run_machine() {
    arguments.clear();
    MachineRunner->kill();
    SpicyRunner->kill();
}
