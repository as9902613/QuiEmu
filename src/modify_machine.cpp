#include "src/modify_machine.h"
#include "ui_modify_machine.h"
#include "src/mainwindow.h"
#include <QDebug>
#include <QErrorMessage>

modify_machine::modify_machine(QString MachineToModify, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::modify_machine) {
        ui->setupUi(this);
        this->MachineToModify = MachineToModify;
        file2.setFileName(MachineToModify);
        file2.open(QIODevice::ReadOnly | QIODevice::Text);
        val2 = file2.readAll();
        file2.close();
        MachineFile2 = QJsonDocument::fromJson(val2.toUtf8());
        MachineHandler2 = MachineFile2.object();
        MachineName2 = MachineHandler2.value(QString("MachineName"));
        SystemType2 = MachineHandler2.value(QString("SystemType"));
        CDImage2 = MachineHandler2.value(QString("CDImage"));
        DiskImage2 = MachineHandler2.value(QString("DiskImage"));
        ramSize2 = MachineHandler2.value("RamSize");
        MachineType = MachineHandler2.value(QString("MachineType"));
        enable_kvm = MachineHandler2.value("EnableKVM").toBool();
        CPU_Type = MachineHandler2.value(QString("CPUType"));
        enable_graphics = MachineHandler2.value("EnableGraphics").toBool();
        enable_spice = MachineHandler2.value("EnableSPICE").toBool();
        SpicePort = MachineHandler2.value("SpicePort");
        GraphicsCard = MachineHandler2.value(QString("GraphicsCard"));
        enable_networking = MachineHandler2.value("EnableNetworking").toBool();
        bypass_host_interface = MachineHandler2.value("BypassHostInterface").toBool();
        NetworkInterface1 = MachineHandler2.value(QString("NetworkInterface1"));
        NetworkInterface2 = MachineHandler2.value(QString("NetworkInterface2"));
        NetworkInterface3 = MachineHandler2.value(QString("NetworkInterface3"));
        enable_samba = MachineHandler2.value("EnableSamba").toBool();
        SambaSharePath = MachineHandler2.value(QString("SambaSharePath"));
        ui->tabWidget->setCurrentIndex(0);
        ui->machine_name_txt->setText(MachineName2.toString());
        ui->system_type_comboBox->setCurrentText(SystemType2.toString());
        if(MachineType.toString()=="q35") {
            ui->machineType_comboBox->setCurrentIndex(0);
        }
        if(MachineType.toString()=="pc") {
            ui->machineType_comboBox->setCurrentIndex(1);
        }
        ui->ram_slider->setValue(ramSize2.toInt());
        ui->ram_size_txt->setText(ramSize2.toString());
        ui->use_existing_image_radio->setChecked(true);
        ui->image_path_txt->setEnabled(true);
        ui->image_path_txt->setText(DiskImage2.toString());
        ui->image_choose_btn->setEnabled(true);
        ui->finish_next_btn->setVisible(false);
        ui->cd_image_path_txt->setText(CDImage2.toString());
        if(enable_kvm==true) {
            ui->enable_kvm_checkbox->setChecked(true);
        }
        if(enable_spice==true) {
            ui->enable_spice_checkbox->setChecked(true);
            ui->spice_port_txt->setText(QString(spice_port));
        }
        if(enable_graphics==true) {
            ui->enable_graphics_checkbox->setChecked(true);
            ui->graphics_card_comboBox->setEnabled(true);
            if(GraphicsCard.toString()=="std") {
                ui->graphics_card_comboBox->setCurrentIndex(0);
            }
            if(GraphicsCard.toString()=="cirrus") {
                ui->graphics_card_comboBox->setCurrentIndex(1);
            }
            if(GraphicsCard.toString()=="vmware") {
                ui->graphics_card_comboBox->setCurrentIndex(2);
            }
            if(GraphicsCard.toString()=="qxl") {
                ui->graphics_card_comboBox->setCurrentIndex(3);
            }
        } else {
            ui->enable_graphics_checkbox->setChecked(false);
            ui->graphics_card_comboBox->setEnabled(false);
        }
        if(enable_networking==true) {
            ui->enable_Networking_checkBox->setChecked(true);
            ui->enable_user_if_passthrough_checkBox->setEnabled(true);
            ui->if1Label->setEnabled(true);
            ui->if2Label->setEnabled(true);
            ui->if3Label->setEnabled(true);
            ui->enable_samba_share_checkBox->setEnabled(true);
            ui->enable_if1_checkBox->setEnabled(true);
            ui->enable_if2_checkBox->setEnabled(true);
            ui->enable_if3_checkBox->setEnabled(true);
            if(!NetworkInterface1.toString().trimmed().isEmpty()) {
                ui->enable_if1_checkBox->setChecked(true);
                ui->if1_comboBox->setEnabled(true);
                if(NetworkInterface1.toString()=="e1000") {
                    ui->if1_comboBox->setCurrentIndex(0);
                }
                if(NetworkInterface1.toString()=="rtl8139") {
                    ui->if1_comboBox->setCurrentIndex(1);
                }
                if(NetworkInterface1.toString()=="virtio") {
                    ui->if1_comboBox->setCurrentIndex(2);
                }

            } else {
                ui->enable_if1_checkBox->setChecked(false);
                ui->if1_comboBox->setEnabled(false);
                ui->if1_comboBox->setCurrentText("");
            }
            if(!NetworkInterface2.toString().trimmed().isEmpty()) {
                ui->enable_if2_checkBox->setChecked(true);
                ui->if2_comboBox->setEnabled(true);
                if(NetworkInterface2.toString()=="e1000") {
                    ui->if2_comboBox->setCurrentIndex(0);
                }
                if(NetworkInterface2.toString()=="rtl8139") {
                    ui->if2_comboBox->setCurrentIndex(1);
                }
                if(NetworkInterface2.toString()=="virtio") {
                    ui->if2_comboBox->setCurrentIndex(2);
                }
            } else {
                ui->enable_if2_checkBox->setChecked(false);
                ui->if2_comboBox->setEnabled(false);
                ui->if2_comboBox->setCurrentText(NetworkInterface2.toString());
            }
            if(!NetworkInterface3.toString().trimmed().isEmpty()) {
                ui->enable_if3_checkBox->setChecked(true);
                ui->if3_comboBox->setEnabled(true);
                if(NetworkInterface3.toString()=="e1000") {
                    ui->if3_comboBox->setCurrentIndex(0);
                }
                if(NetworkInterface3.toString()=="rtl8139") {
                    ui->if3_comboBox->setCurrentIndex(1);
                }
                if(NetworkInterface3.toString()=="virtio") {
                    ui->if3_comboBox->setCurrentIndex(2);
                }
            } else {
                ui->enable_if3_checkBox->setChecked(false);
                ui->if3_comboBox->setEnabled(false);
                ui->if3_comboBox->setCurrentText(NetworkInterface3.toString());
            }
            if(bypass_host_interface) {
                ui->enable_user_if_passthrough_checkBox->setChecked(true);
            } else {
                ui->enable_user_if_passthrough_checkBox->setChecked(false);
              }
        } else {
            ui->enable_Networking_checkBox->setChecked(false);
            ui->enable_if1_checkBox->setEnabled(false);
            ui->enable_if2_checkBox->setEnabled(false);
            ui->enable_if3_checkBox->setEnabled(false);
            ui->enable_user_if_passthrough_checkBox->setEnabled(false);
            ui->if1Label->setEnabled(false);
            ui->if2Label->setEnabled(false);
            ui->if3Label->setEnabled(false);
            ui->enable_samba_share_checkBox->setEnabled(false);
            ui->if1_comboBox->setEnabled(false);
            ui->if2_comboBox->setEnabled(false);
            ui->if3_comboBox->setEnabled(false);
            ui->enable_user_if_passthrough_checkBox->setChecked(false);
        }
        if(enable_samba==true) {
            ui->enable_samba_share_checkBox->setEnabled(true);
            ui->enable_samba_share_checkBox->setChecked(true);
            ui->smb_path_label->setEnabled(true);
            ui->samba_share_path->setEnabled(true);
            ui->samba_share_path->setText(SambaSharePath.toString());
        } else {
            ui->enable_samba_share_checkBox->setChecked(false);
            ui->smb_path_label->setEnabled(false);
            ui->samba_share_path->setEnabled(false);
        }
        if(CPU_Type.toString()=="host") {
            // Bypass Host (needs KVM)
            ui->cpu_model_comboBox->setCurrentIndex(0);
        }
        if(CPU_Type.toString()=="Opteron_G4") {
            // AMD Opteron 62xx (Gen 4 Class Opteron)
            ui->cpu_model_comboBox->setCurrentIndex(1);
        }
        if(CPU_Type.toString()=="Opteron_G3") {
            // AMD Opteron 23xx (Gen 3 Class Opteron)
            ui->cpu_model_comboBox->setCurrentIndex(2);
        }
        if(CPU_Type.toString()=="Opteron_G2") {
            // AMD Opteron 22xx (Gen 2 Class Opteron)
            ui->cpu_model_comboBox->setCurrentIndex(3);
        }
        if(CPU_Type.toString()=="Opteron_G1") {
            // AMD Opteron 240 (Gen 1 Class Opteron)
            ui->cpu_model_comboBox->setCurrentIndex(4);
        }
        if(CPU_Type.toString()=="SandyBridge") {
            // Intel Xeon E312xx (Sandy Bridge)
            ui->cpu_model_comboBox->setCurrentIndex(5);
        }
        if(CPU_Type.toString()=="Nehalem") {
            // Intel Core i7 9xx (Nehalem Class Core i7)
            ui->cpu_model_comboBox->setCurrentIndex(6);
        }
        if(CPU_Type.toString()=="Penryn") {
            // Intel Core 2 Duo P9xxx (Penryn Class Core 2)
            ui->cpu_model_comboBox->setCurrentIndex(7);
        }
        if(CPU_Type.toString()=="Conroe") {
            // Intel Celeron_4x0 (Conroe/Merom Class Core 2)
            ui->cpu_model_comboBox->setCurrentIndex(8);
        }
        if(CPU_Type.toString()=="Westmere") {
            // Westmere E56xx/L56xx/X56xx (Nehalem-C)
            ui->cpu_model_comboBox->setCurrentIndex(9);
        }
        if(CDImage2.toString()=="") {
            ui->cd_image_path_txt->setEnabled(false);
            ui->choose_cdrom_btn->setEnabled(false);
            ui->cd_image_path_txt->setText("");
            ui->choose_cd_image_label->setEnabled(false);
        }
        if(CDImage2.toString()!="") {
            ui->enable_cd_checkbox->setChecked(true);
            ui->cd_image_path_txt->setText(CDImage2.toString());
        }
        if(ui->enable_graphics_checkbox->isChecked()) {
            ui->graphics_card_label->setEnabled(true);
            ui->graphics_card_comboBox->setEnabled(true);
            ui->graphics_card_comboBox->setCurrentText(GraphicsCard.toString());
        }
        if(!ui->enable_graphics_checkbox->isChecked()) {
            ui->graphics_card_label->setEnabled(false);
            ui->graphics_card_comboBox->setEnabled(false);
        }
        if(ui->enable_spice_checkbox->isChecked()) {
            ui->spice_port_txt->setEnabled(true);
            ui->spice_port_label->setEnabled(true);
            ui->spice_port_txt->setText(SpicePort.toString());
        }
        if(!ui->enable_spice_checkbox->isChecked()) {
            ui->spice_port_label->setEnabled(false);
            ui->spice_port_txt->setEnabled(false);
        }
}

modify_machine::~modify_machine() {
    delete ui;
}

void modify_machine::on_ram_slider_valueChanged(int value) {
    ui->ram_size_txt->setText(QString::number(value));
}

void modify_machine::on_apply_btn_clicked() {
    file2.setFileName(MachineToModify);
    file2.remove();
    MachineName2 = ui->machine_name_txt->text();
    if(ui->system_type_comboBox->currentIndex()==0) {
        SystemType2 = "Linux";
    }
    if(ui->system_type_comboBox->currentIndex()==1) {
        SystemType2 = "BSD";
    }
    if(ui->system_type_comboBox->currentIndex()==2) {
        SystemType2 = "OSX";
    }
    if(ui->system_type_comboBox->currentIndex()==3) {
        SystemType2 = "Solaris";
    }
    if(ui->system_type_comboBox->currentIndex()==4) {
        SystemType2 = "Windows";
    }
    ramSize2 = ui->ram_size_txt->text().toInt();
    if(ui->machineType_comboBox->currentIndex()==0) {
        MachineType = "q35";
    }
    if(ui->machineType_comboBox->currentIndex()==1) {
        MachineType = "pc";
    }
    if(ui->cpu_model_comboBox->currentIndex()==0 && !ui->enable_kvm_checkbox->isChecked()) {
        QErrorMessage KVMError(this);
        KVMError.showMessage("Bypassing the Host CPU to the Guest needs KVM enabled!");
        KVMError.exec();
        ui->enable_kvm_checkbox->setChecked(false);
    }
    if(!ui->enable_kvm_checkbox->isChecked() && ui->cpu_model_comboBox->currentIndex()==0) {
        QErrorMessage KVMError(this);
        KVMError.showMessage("Bypassing the Host CPU to the Guest needs KVM enabled!");
        KVMError.exec();
        ui->enable_kvm_checkbox->setChecked(false);
    }
    if(ui->enable_kvm_checkbox->isChecked()) {
        enable_kvm = true;
    }
    else {
        enable_kvm = false;
    }
    switch (ui->cpu_model_comboBox->currentIndex()) {
        case 0:
        // Bypass Host (needs KVM)
            CPU_Type = "host";
        break;
        case 1:
        // AMD Opteron 62xx (Gen 4 Class Opteron)
            CPU_Type = "Opteron_G4";

        break;
        case 2:
        // AMD Opteron 23xx (Gen 3 Class Opteron)
            CPU_Type = "Opteron_G3";

        break;
        case 3:
        // AMD Opteron 22xx (Gen 2 Class Opteron)
            CPU_Type = "Opteron_G2";

        break;
        case 4:
        // AMD Opteron 240 (Gen 1 Class Opteron)
            CPU_Type = "Opteron_G1";

        break;
        case 5:
        // Intel Xeon E312xx (Sandy Bridge)
            CPU_Type = "SandyBridge";

        break;
        case 6:
        // Intel Core i7 9xx (Nehalem Class Core i7)
            CPU_Type = "Nehalem";

        break;
        case 7:
        // Intel Core 2 Duo P9xxx (Penryn Class Core 2)
            CPU_Type = "Penryn";

        break;
        case 8:
        // Intel Celeron_4x0 (Conroe/Merom Class Core 2)
            CPU_Type = "Conroe";

        break;
        case 9:
        // Westmere E56xx/L56xx/X56xx (Nehalem-C)
            CPU_Type = "Westmere";

        break;
    }

    DiskImage2 = ui->image_path_txt->text();
    ramSize2 = ui->ram_size_txt->text().toInt();

    if(ui->enable_graphics_checkbox->isChecked()) {
        enable_graphics = true;
        switch(ui->graphics_card_comboBox->currentIndex()) {
            case 0:
                GraphicsCard = "std";
            break;
            case 1:
                GraphicsCard = "cirrus";
            break;
            case 2:
                GraphicsCard = "vmware";
            break;
            case 3:
                GraphicsCard = "qxl";
            break;
        }
    }
    if(ui->enable_spice_checkbox->isChecked()) {
        enable_spice = true;
        spice_port = ui->spice_port_txt->text().toInt();
    } else {
        enable_spice = false;
        spice_port = 0;
    }
    if(!ui->enable_graphics_checkbox->isChecked()) {
        enable_graphics = false;
        GraphicsCard = "";
    }
    if(ui->enable_Networking_checkBox->isChecked()) {
        enable_networking = true;
        if(ui->enable_if1_checkBox->isChecked()) {
            switch(ui->if1_comboBox->currentIndex()) {
                case 0:
                    NetworkInterface1 = "e1000";
                break;
                case 1:
                    NetworkInterface1 = "rtl8139";
                break;
                case 2:
                    NetworkInterface1 = "virtio";
                break;
            }
        } else {
            NetworkInterface1 = "";
        }
        if(ui->enable_if2_checkBox->isChecked()) {
            switch(ui->if2_comboBox->currentIndex()) {
                case 0:
                    NetworkInterface2 = "e1000";
                break;
                case 1:
                    NetworkInterface2 = "rtl8139";
                break;
                case 2:
                    NetworkInterface2 = "virtio";
                break;
            }
        } else {
            NetworkInterface2 = "";
        }

        if(ui->enable_if3_checkBox->isChecked()) {
            switch(ui->if3_comboBox->currentIndex()) {
                case 0:
                    NetworkInterface3 = "e1000";
                break;
                case 1:
                    NetworkInterface3 = "rtl8139";
                break;
                case 2:
                    NetworkInterface3 = "virtio";
                break;
            }
        }
        else {
            NetworkInterface3 = "";
        }

        if(ui->enable_user_if_passthrough_checkBox->isChecked()) {
            bypass_host_interface = true;
        } else {
            bypass_host_interface = false;
        }

    } else {
        enable_networking = false;
        NetworkInterface1 = "";
        NetworkInterface2 = "";
        NetworkInterface3 = "";
    }

    CDImage2 = ui->cd_image_path_txt->text();
    writeConfig ConfigWriter2;
    ConfigWriter2.write2Config(MachineToModify, MachineName2.toString(), SystemType2.toString(), CDImage2.toString(), DiskImage2.toString(), ramSize2.toInt(), MachineType.toString(), enable_kvm, CPU_Type.toString(), enable_spice, spice_port, enable_graphics, GraphicsCard.toString(), enable_networking, NetworkInterface1.toString(), NetworkInterface2.toString(), NetworkInterface3.toString(), bypass_host_interface);
    QFile::rename(MachineToModify, QString(homedir) + "/.config/QuiEmu/Machines/" + MachineName2.toString() + ".json");
}




void modify_machine::on_ram_size_txt_textChanged(const QString &ramSize2) {
    ui->ram_slider->setValue(ramSize2.toInt());
}

void modify_machine::on_create_new_diskimage_radio_clicked() {
    ui->image_path_txt->setEnabled(false);
    ui->finish_next_btn->setVisible(true);
}

void modify_machine::on_use_existing_image_radio_clicked() {
    ui->image_path_txt->setEnabled(true);
    ui->image_choose_btn->setEnabled(true);
    ui->finish_next_btn->setVisible(false);
}

void modify_machine::on_image_choose_btn_clicked() {
    QFileDialog dialog3;
    dialog3.setFileMode(QFileDialog::ExistingFile);
    DiskImage2 = dialog3.getOpenFileName(this, tr("Select File"), QDir::currentPath(), tr("Disk Images (*.qcow2 *.vdi *.img)"));
    ui->image_path_txt->setText(DiskImage2.toString());
}

void modify_machine::on_choose_cdrom_btn_clicked() {
    QFileDialog dialog4;
    dialog4.setFileMode(QFileDialog::ExistingFile);
    CDImage2 = dialog4.getOpenFileName(this, tr("Select File"), QDir::currentPath(), tr("CD-ROM Images (*.iso *.img *.dmg)"));
    ui->cd_image_path_txt->setText(CDImage2.toString());
}

void modify_machine::on_enable_cd_checkbox_toggled(bool checked) {
    if(checked) {
        ui->cd_image_path_txt->setEnabled(true);
        ui->choose_cdrom_btn->setEnabled(true);
        ui->choose_cd_image_label->setEnabled(true);
    }
    else {
        ui->cd_image_path_txt->setEnabled(false);
        ui->choose_cdrom_btn->setEnabled(false);
        ui->choose_cd_image_label->setEnabled(false);
    }
}

void modify_machine::on_enable_graphics_checkbox_toggled(bool checked) {
    if(checked) {
        enable_graphics = true;
        ui->graphics_card_label->setEnabled(true);
        ui->graphics_card_comboBox->setEnabled(true);
    }
    else {
        enable_graphics = false;
        ui->graphics_card_label->setEnabled(false);
        ui->graphics_card_comboBox->setEnabled(false);
    }
}

void modify_machine::on_enable_spice_checkbox_toggled(bool checked) {
    if(checked) {
        enable_spice = true;
        ui->spice_port_label->setEnabled(true);
        ui->spice_port_txt->setEnabled(true);
    }
    else {
        enable_spice = false;
        ui->spice_port_label->setEnabled(false);
        ui->spice_port_txt->setEnabled(false);
    }
}

void modify_machine::on_exit_btn_clicked() {
    MainWindow *myMainWindow = new MainWindow();
    this->close();
    myMainWindow->show();
}

void modify_machine::on_enable_Networking_checkBox_toggled(bool checked) {
    if(checked) {
        enable_networking = true;
        ui->enable_if1_checkBox->setEnabled(true);
        ui->enable_if2_checkBox->setEnabled(true);
        ui->enable_if3_checkBox->setEnabled(true);
        ui->enable_user_if_passthrough_checkBox->setEnabled(true);
        ui->enable_samba_share_checkBox->setEnabled(true);
        ui->if1Label->setEnabled(false);
        ui->if2Label->setEnabled(false);
        ui->if3Label->setEnabled(false);
    }
    else {
        enable_networking = false;
        ui->enable_if1_checkBox->setEnabled(false);
        ui->enable_if2_checkBox->setEnabled(false);
        ui->enable_if3_checkBox->setEnabled(false);
        ui->enable_user_if_passthrough_checkBox->setEnabled(false);
        ui->enable_samba_share_checkBox->setEnabled(false);
        ui->if1Label->setEnabled(false);
        ui->if2Label->setEnabled(false);
        ui->if3Label->setEnabled(false);
        ui->if1_comboBox->setEnabled(false);
        ui->if2_comboBox->setEnabled(false);
        ui->if3_comboBox->setEnabled(false);
    }
}

void modify_machine::on_enable_if1_checkBox_toggled(bool checked) {
    if(checked) {
        ui->if1Label->setEnabled(true);
        ui->if1_comboBox->setEnabled(true);
    } else {
        ui->if1Label->setEnabled(false);
        ui->if1_comboBox->setEnabled(false);
    }
}

void modify_machine::on_enable_if2_checkBox_toggled(bool checked) {
    if(checked) {
        ui->if2Label->setEnabled(true);
        ui->if2_comboBox->setEnabled(true);
    } else {
        ui->if2Label->setEnabled(false);
        ui->if2_comboBox->setEnabled(false);
    }
}

void modify_machine::on_enable_if3_checkBox_toggled(bool checked) {
    if(checked) {
        ui->if3Label->setEnabled(true);
        ui->if3_comboBox->setEnabled(true);
    } else {
        ui->if3Label->setEnabled(false);
        ui->if3_comboBox->setEnabled(false);
    }
}

void modify_machine::on_enable_samba_share_checkBox_toggled(bool checked) {
    if(checked) {
        ui->samba_share_path->setEnabled(true);
        ui->smb_path_label->setEnabled(true);
    } else {
        ui->samba_share_path->setEnabled(false);
        ui->smb_path_label->setEnabled(false);
    }
}
