#ifndef CREATE_MACHINE2_H
#define CREATE_MACHINE2_H

#include <QApplication>
#include <QWidget>
#include <QtGui>
#include "choose_cd_image.h"

namespace Ui {
class create_machine2;
}

class create_machine2 : public QWidget {
    Q_OBJECT

public:
    explicit create_machine2(QWidget *parent = 0);
    ~create_machine2();
    choose_cd_image *myChoose_cd_imageWindow;

private slots:
    void on_ram_slider_valueChanged(int value);

    void on_previous_btn_clicked();

    void on_cancel_btn_clicked();

    void on_next_btn_clicked();

    void on_ram_size_txt_textChanged(const QString &arg1);

private:
    Ui::create_machine2 *ui;
};

#endif // CREATE_MACHINE2_H
