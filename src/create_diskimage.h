#ifndef CREATE_DISKIMAGE_H
#define CREATE_DISKIMAGE_H

#include <QApplication>
#include <QWidget>
#include <QtGui>
#include <QFileDialog>

namespace Ui {
class create_diskimage;
}

class create_diskimage : public QWidget
{
    Q_OBJECT

public:
    explicit create_diskimage(QWidget *parent = 0);
    ~create_diskimage();
    QString DestinationFileName;

private slots:
    void on_choose_location_btn_clicked();

    void on_create_image_btn_clicked();

private:
    Ui::create_diskimage *ui;
};

#endif // CREATE_DISKIMAGE_H
